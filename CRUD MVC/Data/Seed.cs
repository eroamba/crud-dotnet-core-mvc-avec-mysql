﻿using CRUD_MVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace CRUD_MVC.Data
{
    public class Seed
    {
        public static async Task SeedData(DataContext context)
        {

            var contacts = new List<Contact>{
                new Contact{
                     Id=1,
                     FirstName="Roamba",
                     LastName="Windlassida Epaphras",
                     Phone="+225 0103601135",
                     Email="eroamba@yahoo.fr",
                },
                new Contact{
                     Id=2,
                     FirstName="Djama",
                     LastName="Lemec",
                     Phone="+225 0779157835",
                     Email="djama@gmail.com",
                },

            };

            foreach (var item in contacts )
            {
                 context.Contacts.Add(item);
            }

             await context.SaveChangesAsync();
        } 
        }
}
