﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CRUD_MVC.Models
{
    public class Contact
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Le nom est requis.")]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "Le prénom est requis.")]
        public string LastName { get; set; }
        [Required(ErrorMessage = "Le numéro de téléphone est requis.")]

        public string Phone { get; set; }

        public string Email { get; set; }

    }
}
